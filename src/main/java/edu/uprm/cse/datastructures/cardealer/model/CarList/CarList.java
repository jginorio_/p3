package edu.uprm.cse.datastructures.cardealer.model.CarList;

import java.util.Comparator;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.TwoThreeTree;

public class CarList {
	private static TwoThreeTree<Long, Car> cList = new TwoThreeTree<Long, Car>(new LongComparator());
	private CarList(){}
	
	public static TwoThreeTree<Long, Car> getInstance(){
	    return cList;
	}
	
	public static void resetCars() {
		cList.makeEmpty();
	}
	
	private static class LongComparator implements Comparator<Long> {
		
		public LongComparator() {}

		@Override
		public int compare(Long o1, Long o2) {
			return o1.compareTo(o2);
		}
		
	}
	
}
