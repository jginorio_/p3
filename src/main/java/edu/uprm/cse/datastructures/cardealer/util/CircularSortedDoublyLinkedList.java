package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.comparators.comparator;
import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	
	
	//Node class
	private class Node<E> {
		private Node<E> previousNode;
		private Node<E> nextNode;
		private E element;		
		
		public Node(E element) {
			this.setElement(element);
		}
		
		public Node<E> getPreviousNode() {
			return this.previousNode;
		}
		
		public Node<E> getNextNode() {
			return this.nextNode;
		}
		
		public E getElement() {
			return this.element;
		}
		
		public void setPreviousNode(Node<E> newNode) {
			this.previousNode = newNode;
		}
		
		public void setNextNode(Node<E> newNode) {
			this.nextNode = newNode;
		}
		
		public void setElement(E element) {
			this.element = element;
		}
		
		public String toString() {
			return String.valueOf(this.element);
		}
	}
	
	
	//Iterator class
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E> {

		private Node<E> nextNode;
		
		@SuppressWarnings("unchecked")
		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>)header.getNextNode();
		}
		
		@Override
		public boolean hasNext() {
			return !this.nextNode.equals(header);
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNextNode();
				return result;
			} else {
				throw new NoSuchElementException();
			}
		}
		
	}
	
	
	
	
	private Node<E> header;
	private int currentSize;
	private Comparator comparator;
	
	
	public CircularSortedDoublyLinkedList(Comparator comparator) {
		this.header = new Node(null);
		this.header.setNextNode(this.header);
		this.header.setPreviousNode(this.header);
		this.currentSize = 0;
		this.comparator = comparator;
	}

	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean add(E element) {
		if(this.contains(element)) {
			this.remove(element);
		}
		
		Node<E> newNode = new Node(element);
		
		if(this.isEmpty()) {
			this.header.setNextNode(newNode);
			this.header.setPreviousNode(newNode);
			newNode.setNextNode(this.header);
			newNode.setPreviousNode(this.header);
		} else {
			
			int result = comparator.compare(this.header.getPreviousNode().getElement(), element);
			
			//If the car we are about to add is greater than the last value, then add it last.
			if(result <= 0) {
				Node<E> temp = this.header.getPreviousNode();
				
				temp.setNextNode(newNode);
				
				this.header.setPreviousNode(newNode);
				
				newNode.setPreviousNode(temp);
				newNode.setNextNode(this.header);
			} 
			//If the car we are about to add is less than the last car, then check for the car that is less than the one we are about to add.
			else {
				Node<E> focusNode = this.header.getPreviousNode();
			
				while(focusNode.getElement() != null) {
					
					if(comparator.compare(focusNode.getElement(), element) <= 0)
						break;
					
					focusNode = focusNode.getPreviousNode();
				}
				Node<E> temp = focusNode.getNextNode();
				temp.setPreviousNode(newNode);
				
				newNode.setNextNode(temp);
				newNode.setPreviousNode(focusNode);
				
				focusNode.setNextNode(newNode);
			}
			
			
		}		
		
		this.currentSize++;
		
		return true;
	}

	@Override
	public int size() {
		return this.currentSize;
	}
	

	@Override
	public boolean remove(Object obj) {
		int indexToRemove = this.firstIndex(obj);
		
		if(indexToRemove < 0)
			return false;
		
		return this.remove(indexToRemove);
	}

	@Override
	public boolean remove(int index) {
		if(index < 0 || index >= this.currentSize)
			throw new IndexOutOfBoundsException("Index must be > 0 or < currentSize-1");
		
		Node<E> focusNode;
		int counter = 0;
		for(focusNode = this.header.getNextNode(); counter != index; focusNode = focusNode.getNextNode(), counter++);
		
		
		Node<E> previous = focusNode.getPreviousNode();
		Node<E> next = focusNode.getNextNode();
		
		previous.setNextNode(next);
		next.setPreviousNode(previous);
		this.currentSize--;
		
		focusNode = null;
		
		return true;
	}

	@Override
	public int removeAll(Object obj) {
		int counter = 0;
		
		while(this.remove(obj)) {
			counter++;
		}
		return counter;
	}

	@Override
	public E first() {
		return (E) this.header.getNextNode().getElement();
	}

	@Override
	public E last() {
		return (E) this.header.getPreviousNode().getElement();
	}

	@Override
	public E get(int index) {
		if(index < 0 || index >= this.currentSize)
			throw new IndexOutOfBoundsException("Index must be > 0 or < currentSize-1");
		
		Node<E> focusNode;
		int counter = 0;
		for(focusNode = this.header.getNextNode(); counter != index; focusNode = focusNode.getNextNode(), counter++);
		return focusNode.getElement();
	}

	@Override
	public void clear() {
		Node<E> focusNode = this.header.getNextNode();
		
		while(!focusNode.equals(this.header)) {
			this.remove(focusNode.getElement());
			
			focusNode = focusNode.getNextNode();
		}
	}

	@Override
	public boolean contains(Object e) {
		
		Node<E> focusNode = this.header.getNextNode();
		
		
		while(focusNode.getElement() != null) {
			
			if(focusNode.getElement().equals(e))
				return true;
			
			focusNode = focusNode.getNextNode();
		}
		
		
		return false;
	}

	@Override
	public boolean isEmpty() {
		return this.size() < 1;
	}

	@Override
	public int firstIndex(Object e) {
		int result = 0;
		Node<E> focusNode = this.header.getNextNode();
		
		while(focusNode.getElement() != null) {
			
			if(focusNode.getElement().equals(e))
				return result;
			
			focusNode = focusNode.getNextNode();
			result++;
		}
		
		return -1;
	}

	@Override
	public int lastIndex(Object e) {
		int result = this.size()-1;
		Node<E> focusNode = this.header.getPreviousNode();
		
		while(focusNode.getElement() != null) {
			
			if(focusNode.getElement().equals(e))
				return result;
			
			focusNode = focusNode.getPreviousNode();
			result--;
		}
		
		return -1;
	}
	
	
	public boolean isInUse() {
		return !this.isEmpty();
	}

}
