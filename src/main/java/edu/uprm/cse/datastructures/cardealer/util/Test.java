package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;

//import edu.uprm.cse.datastructures.cardealer.comparators.comparator;
//import edu.uprm.cse.datastructures.cardealer.model.Car;

public class Test {

	public static void main(String[] args) {
		TwoThreeTree<Integer, Integer> arbol = new TwoThreeTree<Integer, Integer>(new IntegerComparator());
		
//		Car zero = new Car(50, "Lamborghini", "Aventador", "A", 100000);
//		Car uno = new Car(60, "Lamborghini", "Aventador", "AB", 200000);
//		Car dos = new Car(70, "Lamborghini", "Aventador", "ABC", 300000);
//		Car tres = new Car(40, "Lamborghini", "Aventador", "ABCD", 400000);
//		Car cuatro = new Car(30, "Lamborghini", "Aventador", "ABCDE", 500000);
//		Car cinco = new Car(20, "Lamborghini", "Aventador", "ABCDE", 500000);
//		Car seis = new Car(10, "Lamborghini", "Aventador", "ABCDEF", 500000);
//		Car siete = new Car(80, "Lamborghini", "Aventador", "ABCDEF", 500000);
//		Car ocho = new Car(90, "Lamborghini", "Aventador", "ABCDEFa", 500000);
//		Car diez = new Car(100, "Lamborghini", "Aventador", "ABCDEFa", 500000);
//		Car once = new Car(200, "Lamborghini", "Aventador", "ABCDEFa", 500000);
		
		arbol.put(50, 50);
		arbol.put(60, 60);
		arbol.put(70, 70);
		arbol.put(40, 40);
		arbol.put(30, 30);
		arbol.put(20, 20); 
		arbol.put(10, 10);
		arbol.put(80, 80);
		arbol.put(90, 90); 
		arbol.put(100, 100);
		arbol.put(200, 200);
		arbol.put(55, 55);
		arbol.put(300, 300);
		arbol.put(400, 400);
		arbol.put(5, 5);
		arbol.put(1, 1);
		arbol.put(2, 2);
		
		arbol.put(0, 0);
//		arbol.put(0, 4);
//		arbol.put(0, 3);
//		arbol.put(0, 2);
//		arbol.put(0, 1);
		arbol.remove(0);
		arbol.put(0, 0);
		
		arbol.put(24, 24);
		//https://www.cs.usfca.edu/~galles/visualization/BTree.html
		arbol.print();
		
		System.out.println();
//		
//		System.out.println(arbol.size());
		
		for(Integer I: arbol.getKeys()) {
			System.out.print(I + ", ");
		}
	}
	

	public static class IntegerComparator implements Comparator<Integer> {
		
		public IntegerComparator() {
			
		}

		@Override
		public int compare(Integer o1, Integer o2) {
			return o1.compareTo(o2);
		}
		
	}

}


