package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;

import edu.uprm.cse.datastructures.cardealer.util.BTree.TreeNode;


@SuppressWarnings("unused")
public abstract class BTree<K,V> implements Map<K,V>{

    int currentSize;
    int deleted;
    TreeNode root;
    Comparator<K> keyComparator;

    class MapEntry implements Comparable<MapEntry>{
        K key;
        V value;
        boolean deleted;
        Comparator<K> keyComparator;
        MapEntry(K key, V value, Comparator<K> keyComparator){
            this.keyComparator = keyComparator;
            this.key = key;
            this.value = value;
            deleted = false;
        }
        @Override
        public int compareTo(MapEntry o) {
            return keyComparator.compare(this.key, o.key);
        }
    }

    class MapEntryComparator implements Comparator<MapEntry>{

        @Override
        public int compare(MapEntry o1, MapEntry o2) {
            return o1.compareTo(o2);
        }
    }

    class TreeNode {
        SortedList<MapEntry> entries;
        TwoThreeTree.TreeNode left, right, center, parent, temp;
        Comparator<K> comparator;
        TreeNode(K keyA, K keyB, V a, V b, Comparator<K> comparator){
            this.comparator = comparator;
            this.entries = new CircularSortedDoublyLinkedList<>(new MapEntryComparator());
            this.entries.add(new MapEntry(keyA, a, comparator));
            if(keyB != null) {
                this.entries.add(new MapEntry(keyB, b, comparator));
            }
        }
        TreeNode(MapEntry entryA, MapEntry entryB, Comparator<K> comparator){
            this.comparator = comparator;
            this.entries = new CircularSortedDoublyLinkedList<>(new MapEntryComparator());
            this.entries.add(entryA);
            if(entryB != null && !entryB.deleted){
                this.entries.add(entryB);
            }
        }
        
        public String toString() {
        	if(this.entries.isEmpty())
        		return "There is no entries";
        	else if(this.entries.size() == 2)
        		return "{ \n "+this.entries.get(0).value+", \n "+this.entries.get(1).value+"\n}";
        	else
        		return "{ "+this.entries.get(0).value +" }";
        }
        
        public boolean hasSpace() {
        	return this.entries.size() < 2 ;
        }
        
        public MapEntry getSmallerValue() {
        	if(this.entries.isEmpty())
        		return null;
        	else
        		return this.entries.get(0);
        }
        
        public MapEntry getLargerValue() {
        	//Si tiene dos values, devuelve el mayor.
        	if(this.entries.size() == 2) {
        		return this.entries.get(1);
        	}
        	//Si tiene solo 1 valor, pues ese valor es el mayor so devuelvelo.
        	return this.getSmallerValue();
        }
        
        public boolean isLeaf() {
        	return this.center == null && this.right == null && this.left == null;
        }
        
        public boolean is2Node() {
        	return this.left != null && this.center == null && this.right != null;
        }
        
        public boolean is3Node() {
        	return this.left != null && this.center != null && this.right != null;
        }
        
        
        /*
         * Subir el del medio despues de splitear un 3 node o una hoja
         */
        @SuppressWarnings("unchecked")
		protected void promote(TreeNode N) {
        	//This node is a 2-node
            if (this.is2Node()) {   

                //Node N.getSmallerValue().key is smaller than this.getSmallerValue().key
                if (this.comparator.compare(this.getSmallerValue().key, N.getSmallerValue().key) > 0) { 
                	this.entries.add(N.getSmallerValue());          
                	this.left = N.left;
                	this.left.parent = (TwoThreeTree.TreeNode) this;
                	this.center = N.right;
                	this.center.parent = (TwoThreeTree.TreeNode) this;
                	
                } 
                //Node N.getSmallerValue().key is larger or equal than this.getSmallerValue().key
                else { 
                	this.entries.add(N.getSmallerValue());
                	this.center = N.left;
                	this.center.parent = (TwoThreeTree.TreeNode) this;
                	this.right = N.right;
                	this.right.parent = (TwoThreeTree.TreeNode) this;
                }
            }
            
            else {          
                threeSplit(N);

                if (parent != null) {
                    parent.promote((TwoThreeTree.TreeNode)this);
                }
            }
        }
        
        /**
         * Splitea un 3 node interno creando un 2 node que se hace promote.
         */
        @SuppressWarnings("unchecked")
		private void threeSplit(TreeNode N) {
            TreeNode temp = null;

            if (this.comparator.compare(this.getSmallerValue().key, N.getSmallerValue().key) > 0) {     
            	this.left = (TwoThreeTree.TreeNode) N;
            	this.left.parent = (TwoThreeTree.TreeNode)this;
            	temp = new TreeNode(this.getLargerValue(), null, this.comparator);
            	temp.parent = (TwoThreeTree.TreeNode)this;
            	this.entries.remove(this.getLargerValue());
            	temp.left = this.center;
            	temp.left.parent = (TwoThreeTree.TreeNode) temp;
            	temp.right = this.right;
            	temp.right.parent = (TwoThreeTree.TreeNode) temp;
            	this.right = (TwoThreeTree.TreeNode) temp;
            	this.center = null;

            } else {
            	//Item to add is between dataOne and dataTwo
                if (this.comparator.compare(this.getLargerValue().key, N.getSmallerValue().key) > 0) {     
                	temp = new TreeNode(this.getSmallerValue(), null, this.comparator);
                	temp.parent = (TwoThreeTree.TreeNode) this;
                	temp.left = this.left;
                	temp.left.parent = (TwoThreeTree.TreeNode) temp;
                	temp.right = N.left;
                	temp.right.parent = (TwoThreeTree.TreeNode) temp;
                	this.left = (TwoThreeTree.TreeNode) temp;
                	temp = new TreeNode(this.getLargerValue(), null, this.comparator);
                	temp.parent = (TwoThreeTree.TreeNode) this;
                	temp.left = N.right;
                	temp.left.parent = (TwoThreeTree.TreeNode) temp;
                	temp.right = this.right;
                	temp.right.parent = (TwoThreeTree.TreeNode) temp;
                	this.right = (TwoThreeTree.TreeNode) temp;
                	this.entries.clear();
                	this.entries.add(N.getSmallerValue());
                	this.center = null;
                } else {          
                	//Item to add is larger than dataTwo
                	this.right = (TwoThreeTree.TreeNode) N;
                	this.right.parent = (TwoThreeTree.TreeNode)this;
                	temp = new TreeNode(this.getSmallerValue(), null, this.comparator);
                	temp.parent = (TwoThreeTree.TreeNode)this;
                	temp.left = this.left;
                	temp.left.parent = (TwoThreeTree.TreeNode)temp;
                	temp.right = this.center;
                	temp.right.parent = (TwoThreeTree.TreeNode)temp;
                	this.left = (TwoThreeTree.TreeNode)temp;
                	this.entries.remove(this.getSmallerValue());
                	this.center = null;
                }
            }

        }
    }

    public BTree(Comparator<K> keyComparator){
        this.currentSize = 0;
        this.deleted = 0;
        this.keyComparator = keyComparator;
    }

    abstract boolean isLeaf(TreeNode treeNode);

    /*
        splits a node with 3 keys
    */
    abstract void split(TreeNode treeNode, MapEntry newValue);


    public void print() {
        this.printAux(this.root, 0);
    }

    @SuppressWarnings("unchecked")
	private void printAux(TreeNode N, int i) {
        if (N != null) {
            this.printAux((BTree<K, V>.TreeNode) N.right, i + 4);
            if(N.entries.size() > 1) {
                System.err.print((i/4)+"|");
                for (int j=0; j < i; ++j) {
                    System.err.print(" ");
                }
                System.err.println(N.entries.last().value);
            }
            this.printAux((BTree<K, V>.TreeNode) N.center, i + 4);
            System.err.print((i/4)+"|");
            for (int j=0; j < i; ++j) {
                System.err.print(" ");
            }

            System.err.println(N.entries.first().value);

            this.printAux((BTree<K, V>.TreeNode) N.left, i + 4);
        }

    }



}
