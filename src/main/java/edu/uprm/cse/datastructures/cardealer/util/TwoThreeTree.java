package edu.uprm.cse.datastructures.cardealer.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import edu.uprm.cse.datastructures.cardealer.model.Car;


public class TwoThreeTree<K, V> extends BTree<K, V>{

	public TwoThreeTree(Comparator<K> keyComparator) {
		super(keyComparator);
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize < 1;
	}

	@Override
	public V get(K key) {
		if(key == null)
			throw new IllegalArgumentException("Key can NOT be null");
		

		TreeNode focusNode = this.root;
		
		while(this.keyComparator.compare(focusNode.getSmallerValue().key, key) != 0 && this.keyComparator.compare(focusNode.getLargerValue().key, key) != 0 ) {	
			//Si el nuevo item es menor que el menor de focus, entra x la izquierda
			if(this.keyComparator.compare(focusNode.getSmallerValue().key, key) > 0) {
				focusNode = focusNode.left;
				
			} else if(focusNode.is2Node()) {
				focusNode = focusNode.right;
				
			} else {
				//Si el nuevo item es menor que el mayor de focus, entra x el medio
				if(this.keyComparator.compare(focusNode.getLargerValue().key, key) > 0) {				
					focusNode = focusNode.center;
						
				} else {
					focusNode = focusNode.right;						
				}
			}
			
			if(focusNode == null) {
				return null;
			}
		}
		
		V result = null;
		
		//Si es un two node, solo tiene un valor so devuelve el primero y verifica que no este deleted.
		if(focusNode.is2Node() && !focusNode.entries.first().deleted) {
			result = focusNode.entries.first().value;
		} else {
			//Como es un 3 node, verifica con ambos valores, devuelve el indicado y verifica que no este deleted.
			if(this.keyComparator.compare(focusNode.getSmallerValue().key, key) == 0 && !focusNode.getSmallerValue().deleted)
				result = focusNode.getSmallerValue().value;
			else if(!focusNode.getLargerValue().deleted)
				result = focusNode.getLargerValue().value;		
		}
			
		return result;
	}

	@Override
	public V put(K key, V value) {
		if(key == null)
			throw new IllegalArgumentException("Key can NOT be null");
		if(value == null)
			throw new IllegalArgumentException("Value can NOT be null");
		
		MapEntry map2Add = new MapEntry(key, value, this.keyComparator);
		
		if(this.root == null) {
			this.root = new TreeNode(map2Add, null, this.keyComparator);
			this.currentSize++;
			return value;
		}
		
		//Si key ya existe pero esta deleted para darle update o si solo es update.
		TreeNode focusNode = this.getNonDelete(key);
		if(focusNode != null) {
			//Si es un two node, solo tiene un valor so modifica el primero.
			if(focusNode.is2Node()) {
				if(focusNode.entries.first().deleted)
					this.currentSize++;
				
				focusNode.entries.first().deleted = false;
				focusNode.entries.first().value = value;
			} else {
				//Como es un 3 node, verifica con ambos valores, modifica el indicado.
				if(this.keyComparator.compare(focusNode.getSmallerValue().key, key) == 0) {
					if(focusNode.getSmallerValue().deleted)
						this.currentSize++;
					
					focusNode.getSmallerValue().deleted = false;
					focusNode.getSmallerValue().value = value;
				} else {
					if(focusNode.getLargerValue().deleted)
						this.currentSize++;
					
					focusNode.getLargerValue().deleted = false;
					focusNode.getLargerValue().value = value;	
				}
			}
			
			return value;
		}

		focusNode = this.root;
		TreeNode parentFocus = this.root;
		
		//Buscar el spot donde se pudiese poner el MapEntry
		while(true) {
			
			parentFocus = focusNode;
			
			//Si el nuevo item es menor que el menor de focus, entra x la izquierda
			if(this.keyComparator.compare(focusNode.getSmallerValue().key, map2Add.key) > 0) {
				focusNode = focusNode.left;
				
				if(focusNode == null) {
					break;
				}
				
			} else if(focusNode.is2Node()) {
				focusNode = focusNode.right;
				
				if(focusNode == null) {
					break;
				}
				
			} else {
				//Si el nuevo item es menor que el mayor de focus, entra x el medio
				if(this.keyComparator.compare(focusNode.getLargerValue().key, map2Add.key) > 0) {
					
					focusNode = focusNode.center;
					
					if(focusNode == null) {
						break;
					}
						
				} else {
					focusNode = focusNode.right;
					
					if(focusNode == null) {
						break;
					}
						
				}
			}
		}

		focusNode = parentFocus;


		if(focusNode.isLeaf()) {
			
			if(focusNode.hasSpace()) {
				focusNode.entries.add(map2Add);
				this.currentSize++;
				return value;
			} else {
				this.split(focusNode, map2Add);
				
	            if (focusNode.parent != null) {   
	                focusNode.parent.promote(focusNode);
	            }
				this.currentSize++;
	            return value;
			}

		}

		return null;
	}
	
	
	@Override
	void split(TreeNode treeNode, MapEntry newValue) {		
		//Si el nuevo key es menor que el key menor.
		if(this.keyComparator.compare(treeNode.getSmallerValue().key, newValue.key) > 0) {
	        treeNode.left = new TreeNode(newValue, null, this.keyComparator);
	        treeNode.left.parent = treeNode;
	        
	        treeNode.right = new TreeNode(treeNode.getLargerValue(), null, this.keyComparator);
	        treeNode.right.parent = treeNode;
	        
	        treeNode.entries.remove(treeNode.getLargerValue());
	    } 
		//el nuevo key es mayor que el key en la derecha
		else { 

	        if(this.keyComparator.compare(treeNode.getLargerValue().key, newValue.key) > 0) {    //New item is smaller than largerValue
	        	treeNode.left = new TreeNode(treeNode.getSmallerValue(), null, this.keyComparator);
	        	treeNode.left.parent = treeNode;
	        	
	        	treeNode.right = new TreeNode(treeNode.getLargerValue(), null, this.keyComparator);
	        	treeNode.right.parent = treeNode;
	        	
	        	treeNode.entries.clear();
	        	treeNode.entries.add(newValue);

	        }  else {              //New item is larger than dataTwo and largest
	        	treeNode.left = new TreeNode(treeNode.getSmallerValue(), null, this.keyComparator);
	        	treeNode.left.parent = treeNode;
	        	
	        	treeNode.right = new TreeNode(newValue, null, this.keyComparator);
	        	treeNode.right.parent = treeNode;
	        	
	        	treeNode.entries.remove(treeNode.getSmallerValue());
	        }
	    }	
	}

	@Override
	public V remove(K key) {
		if(key == null)
			throw new IllegalArgumentException("Key can NOT be null");
		

		TreeNode focusNode = this.root;
		
		while(this.keyComparator.compare(focusNode.getSmallerValue().key, key) != 0 && this.keyComparator.compare(focusNode.getLargerValue().key, key) != 0 ) {	
			//Si el nuevo item es menor que el menor de focus, entra x la izquierda
			if(this.keyComparator.compare(focusNode.getSmallerValue().key, key) > 0) {
				focusNode = focusNode.left;
				
			} else if(focusNode.is2Node()) {
				focusNode = focusNode.right;
				
			} else {
				//Si el nuevo item es menor que el mayor de focus, entra x el medio
				if(this.keyComparator.compare(focusNode.getLargerValue().key, key) > 0) {				
					focusNode = focusNode.center;
						
				} else {
					focusNode = focusNode.right;						
				}
			}
			
			if(focusNode == null) {
				return null;
			}
		}
		
		V result = null;
		
		//Si es un two node, solo tiene un valor so devuelve el primero y verifica que no este deleted.
		if(focusNode.is2Node() && !focusNode.entries.first().deleted) {
			result = focusNode.entries.first().value;
			focusNode.entries.first().deleted = true;
		} else {
			//Como es un 3 node, verifica con ambos valores y devuelve el indicado y verifica que no este deleted.
			if(this.keyComparator.compare(focusNode.getSmallerValue().key, key) == 0 && !focusNode.getSmallerValue().deleted) {
				result = focusNode.getSmallerValue().value;
				focusNode.getSmallerValue().deleted = true;
			} else if(!focusNode.getLargerValue().deleted) {
				result = focusNode.getLargerValue().value;	
				focusNode.getLargerValue().deleted = true;
			}
		}
		
		this.currentSize--;
		
		return result;
	}
	
	
	private BTree<K, V>.TreeNode getNonDelete(K key){
		if(key == null)
			throw new IllegalArgumentException("Key can NOT be null");
		

		TreeNode focusNode = this.root;
		
		while(this.keyComparator.compare(focusNode.getSmallerValue().key, key) != 0 && this.keyComparator.compare(focusNode.getLargerValue().key, key) != 0 ) {	
			//Si el nuevo item es menor que el menor de focus, entra x la izquierda
			if(this.keyComparator.compare(focusNode.getSmallerValue().key, key) > 0) {
				focusNode = focusNode.left;
				
			} else if(focusNode.is2Node()) {
				focusNode = focusNode.right;
				
			} else {
				//Si el nuevo item es menor que el mayor de focus, entra x el medio
				if(this.keyComparator.compare(focusNode.getLargerValue().key, key) > 0) {				
					focusNode = focusNode.center;
						
				} else {
					focusNode = focusNode.right;						
				}
			}
			
			if(focusNode == null) {
				return null;
			}
		}
			
		return focusNode;
	}

	@Override
	public boolean contains(K key) {
		if(key == null)
			throw new IllegalArgumentException("Key can NOT be null");
		
		return this.get(key) != null;
	}
	
	private void getKeysAux(TreeNode focusNode, ArrayList<K> arr) {
		if(focusNode == null)
			return;
		//Como es un 2 node solo verifica el delete status del primero

		if(focusNode.isLeaf()) {
			for(MapEntry v: focusNode.entries) {
				arr.add(v.key);
			}
		} else if (focusNode.is2Node()) {
			this.getKeysAux(focusNode.left, arr);
			for(MapEntry v: focusNode.entries) {
				arr.add(v.key);
			}
			this.getKeysAux(focusNode.right, arr);
		} else {
			this.getKeysAux(focusNode.left, arr);
			arr.add(focusNode.getSmallerValue().key);
			this.getKeysAux(focusNode.center, arr);
			arr.add(focusNode.getLargerValue().key);
			this.getKeysAux(focusNode.right, arr);
		}
	}

	@Override
	public List<K> getKeys() {
		ArrayList<K> newArr = new ArrayList<>();
		this.getKeysAux(this.root, newArr);
		return newArr;
	}
	
	
	private void getValuesAux(TreeNode focusNode, ArrayList<V> arr) {
		if(focusNode == null)
			return;
		//Como es un 2 node solo verifica el delete status del primero

		if(focusNode.isLeaf()) {
			for(MapEntry v: focusNode.entries) {
				if(!v.deleted)
					arr.add(v.value);
			}
		} else if (focusNode.is2Node()) {
			this.getValuesAux(focusNode.left, arr);
			for(MapEntry v: focusNode.entries) {
				if(!v.deleted)
					arr.add(v.value);
			}
			this.getValuesAux(focusNode.right, arr);
		} else {
			this.getValuesAux(focusNode.left, arr);
			
			if(!focusNode.getSmallerValue().deleted)
				arr.add(focusNode.getSmallerValue().value);
			
			this.getValuesAux(focusNode.center, arr);
			
			if(!focusNode.getLargerValue().deleted)
				arr.add(focusNode.getLargerValue().value);
			
			this.getValuesAux(focusNode.right, arr);
		}
	
	}
	

	@Override
	public List<V> getValues() {
		ArrayList<V> newArr = new ArrayList<>();
		this.getValuesAux(this.root, newArr);
		return newArr;
	}

	@Override
	boolean isLeaf(BTree<K, V>.TreeNode treeNode) {
		return treeNode.center == null && treeNode.right == null && treeNode.left == null;
	}
	
	public void makeEmpty() {
		this.root = null;
		this.currentSize = 0;
	}
}
